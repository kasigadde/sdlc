package com.maxim.jms.listener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.maxim.jms.adapter.ConsumerAdapter;

@Component	
public class ConsumerListener implements MessageListener {
	
	@Autowired
	JmsTemplate jmsTemplate;
	
	@Autowired
	ConsumerAdapter consumerAdapter;
	
	
	
	public void onMessage(Message message) {
		System.out.println("In onMessage()....");
		
		String json = null;
		
		if(message instanceof TextMessage){
			try {
				json = ((TextMessage) message).getText();
				consumerAdapter.sendToMongo(json);
				
			} catch (JMSException e) {
				jmsTemplate.convertAndSend(json);
			}
		}
	}

}
